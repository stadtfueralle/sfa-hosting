nixos:
let
  ips = {
    defaultGateway = "193.148.249.1";
    address = "193.148.249.132";
    nameservers = [ "45.134.88.88" ];
  };

  basicConfig = (import ./basicConf.nix ips);
  hwConfig = { pkgs, config, lib, ... }: {
    boot.loader.grub.enable = true;
    boot.loader.grub.device = "/dev/vda";
    services.udev.extraRules = ''
      SUBSYSTEM=="net", ACTION=="add", ATTR{address}=="ca:40:e9:df:c1:a2", NAME="eth0"
    '';
  };
in {
  inherit ips hwConfig basicConfig;
  nginxConfig = { pkgs, config, ... }: {
    # TODO checkout section Hardened setup with TLS and HSTS preloading
    # https://nixos.wiki/wiki/Nginx
    networking.firewall.allowedTCPPorts = [ 80 443 ];
    services.nginx = {
      enable = true;
      recommendedTlsSettings = true;
      recommendedProxySettings = true;
      virtualHosts."map.stadtfueralle.info" = {
        forceSSL = true;
        enableACME = true;
        locations = {
          "/static/".alias = pkgs.atlas-static + "/";
          "/sw" = {
            root = pkgs.atlas-static + "/js/";
            tryFiles = "/sw.js /sw.js";
          };
          "/".proxyPass = "http://127.0.0.1:8000";
        };
      };
    };
    security.acme = {
      email = "news@durga.ch";
      acceptTerms = true;
      certs."map.stadtfueralle.info".email = "news@durga.ch";
    };
  };
  iso = (nixos.lib.nixosSystem {
    system = "x86_64-linux";
    modules = [
      "${nixos}/nixos/modules/installer/cd-dvd/iso-image.nix"
      basicConfig hwConfig
      ({...}: {
        isoImage.isoName = "sfa-host.iso";
      })
    ];
  }).config.system.build.isoImage;
}
