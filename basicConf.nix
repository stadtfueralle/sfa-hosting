ips: { pkgs, config, lib, modulesPath, ...}:
{
  imports = [
    (modulesPath + "/profiles/qemu-guest.nix")
    (modulesPath + "/profiles/headless.nix")
    (modulesPath + "/profiles/minimal.nix")
  ];
  environment.systemPackages = [ pkgs.openssl pkgs.vim ];
  fileSystems = {
    "/" = {
      device = "/dev/disk/by-label/nixos";
      neededForBoot = true;
    };
  };
  networking = {
    firewall.enable = true;
    firewall.allowedTCPPorts = [ 62222 ];
    defaultGateway = {
      address = ips.defaultGateway;
      interface = "eth0";
    };
    interfaces.eth0.ipv4.addresses = [{
      address = ips.address;
      prefixLength = 24;
    }];
    nameservers = ips.nameservers; # ifog nameserver
  };
  # fix for colmena apply
  # error: cannot add path '/nix/store/0m3....' because it lacks a valid signature
  nix.settings.trusted-users = [ "root" "@wheel" ];
  nix.settings.experimental-features = [ "nix-command" "flakes" ];
  nixpkgs.hostPlatform = lib.mkDefault "x86_64-linux";
  swapDevices = [{
    device = "/dev/disk/by-label/swap";
  }];
  security.sudo = {
    enable = true;
    wheelNeedsPassword = false;
  };
  services.openssh = {
    enable = true;
    ports = [ 62222 ];
  };
  users.users.sfa-user = {
    isNormalUser = true;
    uid = 1000;
    extraGroups = [ "keys" "wheel" ];
    openssh.authorizedKeys.keys = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOf6dgKch+BWz8MbOG6iovPhQQgrx4GRnm78HR3NnayP (none)"
    ];
  };
}
