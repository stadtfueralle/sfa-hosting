![SFA Logo](server/img/logo.png "Stadt für Alle"){width=100px height=100px}

# SFA Hosting

# General instructions

* [Install Nix](https://nixos.org/manual/nix/stable/installation/installing-binary.html)
* [Enable flakes](https://nixos.wiki/wiki/Flakes#Enable_flakes)
* Clone sfa-hosting repository

```
git clone https://gitlab.com/durgabahadur/sfa-hosting.git
```

* Move to the repository directory

```
cd sfa-hosting
```

* Enter the sfa environment

```
nix develop
```

# Run sfa in a local qemu instance

* First, build the qemu instance:

```
nix build .#localTestingQemu
```

* Create the tuntap link so qemu can access your network (a bridge called br0
  must be present on your system):

```
createBridge
```

* Start qemu

```
./result/bin/run-nixos-vm
```
* Copy the keys needed by SFA Atlas to the `/tmp` directory

Being in the sfa environment, copy a file with the necessary environment
variable definitions to `/tmp/ifogEnv.sh`.

An example file would be:

``` bash
export APIHOST=10.13.12.88
export DBNAME=sfa-data
export DBPASS=whateverItCouldBe
export DBHOST=/run/postgresql
export DBPORT=5432
export DBUSER=postgres
export ENVIRONMENT=production
export OIDC_CLIENT_ID=IdontSay
export OIDC_CLIENT_SECRET=wontTell
```
* Deploy sfa apps to locally running qemu

```
colmena apply --on localQemu
```
