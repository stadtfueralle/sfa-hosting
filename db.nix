{
  legacyDbConfig = { pkgs, ...}: let
    pg10 = {
      tmp  = "/tmp/postgres10";
      data = "/var/lib/postgresql/10";
      dbName = "sfa-legacy";
    };
    legacyConfFile = 
      (pkgs.writeText "postgres.conf" ''
        listen_addresses = '*'
        log_destination = 'stderr'
        log_line_prefix = '[%p] '
        port = 15432
        unix_socket_directories = '${pg10.tmp}'
      '');
    createLegacyDb =
      (pkgs.writeShellScriptBin "createLegacyDb" ''
        ARGS="-U postgres -h ${pg10.tmp} -p 15432 ${pg10.dbName}"
        ${pkgs.pg10}/bin/createdb $ARGS
        ${pkgs.pg10}/bin/psql $ARGS -c "CREATE EXTENSION postgis;"
        ${pkgs.pg10}/bin/psql $ARGS -c "CREATE ROLE mapper1 LOGIN CREATEDB;"
        ${pkgs.pg10}/bin/psql $ARGS -c "CREATE ROLE rapper1 LOGIN CREATEDB;"
        ${pkgs.pg10}/bin/psql $ARGS -c "ALTER USER \"mapper1\" WITH PASSWORD '$MAPPER1_PASS';"
        ${pkgs.pg10}/bin/psql $ARGS -c "ALTER USER \"rapper1\" WITH PASSWORD '$RAPPER1_PASS';"
        # ${pkgs.pg10}/bin/psql $ARGS < $1
      '');
    initLegacyPG = 
      (pkgs.writeShellScriptBin "initLegacyPG" ''
        # execute as user postgres
        mkdir ${pg10.data}
        ${pkgs.pg10}/bin/initdb ${pg10.data} -U postgres --auth=trust --auth-host=md5 --locale en_US.UTF-8 # >/dev/null
        echo "local all all trust" > ${pg10.data}/pg_hba.conf
        echo "host ${pg10.dbName} mapper1 0.0.0.0/0 md5" >> ${pg10.data}/pg_hba.conf
        echo "host ${pg10.dbName} rapper1 0.0.0.0/0 md5" >> ${pg10.data}/pg_hba.conf
      '');
    preStartLegacyPG =
      (pkgs.writeShellScriptBin "preStartLegacyPG" ''
        #!${pkgs.bash}/bin/bash
        set -e
        mkdir -p ${pg10.tmp}
        if ! test -e /var/lib/postgresql/10/PG_VERSION; then
          # Cleanup the data directory.
          # Initialise the database.
          ${initLegacyPG}/bin/initLegacyPG
          cp ${legacyConfFile} ${pg10.data}/postgresql.conf
          # See postStart!
          touch "/var/lib/postgresql/10/.first_startup"
        fi
      '');
    postStartLegacyPG = 
      (pkgs.writeShellScriptBin "postStartLegacyPG" ''
        #!${pkgs.bash}/bin/bash
        set -e
        PSQL="${pkgs.pg10}/bin/psql -h ${pg10.tmp} --port=15432"

        while ! $PSQL -d postgres -c "" 2> /dev/null; do
            if ! kill -0 "$MAINPID"; then exit 1; fi
            sleep 0.1
        done

        if test -e "/var/lib/postgresql/10/.first_startup"; then
          rm -f "/var/lib/postgresql/10/.first_startup"
          ${createLegacyDb}/bin/createLegacyDb
        fi
      ''); in
    {
      environment.systemPackages = [
      ];
      networking = {
        firewall.enable = true;
        firewall.allowedTCPPorts = [ 15432 ];
      };
      systemd.services.legacyDb = {
        enable = true;
        postStart = "${postStartLegacyPG}/bin/postStartLegacyPG";
        preStart = "${preStartLegacyPG}/bin/preStartLegacyPG";
        # script = "${startLegacyPG}/bin/startLegacyPG";
        unitConfig = {
          After="network.target";
          Description="Legacy PostgreSQL Server";
          RequiresMountsFor=/var/lib/postgresql/10;
        };
        serviceConfig = {
          Restart = "always";
          EnvironmentFile = "/run/keys/legacyDbPass";
          ExecReload = "${pkgs.utillinux}/bin/kill -9 $MAINPID";
          ExecStart = "${pkgs.pg10}/bin/postgres -D ${pg10.data}";
          Type = "notify";
          KillMode = "mixed";
          User = "postgres";
          Group = "postgres";
        };
      };
    };
  geoDbConfig = { pkgs, config, ... }: {
    services.postgresql = {
      enable = true;
      package = pkgs.postgresql;
      authentication = pkgs.lib.mkOverride 10 ''
        #type     database  DBuser   IP-address     auth-method
        local     all       all                     trust
      '';
      extraPlugins = with pkgs.postgresql.pkgs; [ postgis ];
      ensureDatabases = ["sfa-data"];
    };
  };
}
