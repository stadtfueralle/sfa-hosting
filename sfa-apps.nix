{
  server = sfaAtlasExec: { pkgs, config, ... }: {
    boot.postBootCommands = ''
        # allow systemd to start user service after boot, no login needed
        loginctl enable-linger sfa-user
    '';
    environment.systemPackages = [ sfaAtlasExec ];
    systemd.services.runserver = {
      enable = true;
      # preStart = ""; # sourcing here does not work, script gets a new environment
      script = "source /run/keys/exportVars && ${sfaAtlasExec.outPath}/bin/run-server";
      serviceConfig = {
        Restart = "always";
        ExecStop = "${pkgs.utillinux}/bin/kill -9 $MAINPID";
        Type = "exec";
        User = "sfa-user";
        Group = "users";
      };
      wantedBy = [ "default.target" ];
    };
  };

  geoDbConfig = { pkgs, config, ... }: {
    services.postgresql = {
      enable = true;
      package = pkgs.postgresql;
      enableTCPIP = false;
      authentication = pkgs.lib.mkOverride 10 ''
        #type     database  DBuser   IP-address     auth-method
        local     all       all                     trust
      '';
      extraPlugins = with pkgs.postgresql.pkgs; [ postgis ];
      ensureDatabases = ["sfa-data"];
    };
  };

  nginxTestConfig = staticFiles: { pkgs, config, ... }: {
    networking.firewall.allowedTCPPorts = [ 80 ];
    networking.nameservers = [ "9.9.9.9" ]; # quad9 nameserver
    services.nginx = {
      enable = true;
      recommendedProxySettings = true;
      virtualHosts."localhost".locations = {
        "/static/".alias = staticFiles + "/";
        "/".proxyPass = "http://localhost:8000";
      };
    };
  };

  nginxIfogConfig = staticFiles: { pkgs, config, ... }: {
    # TODO checkout section Hardened setup with TLS and HSTS preloading
    # https://nixos.wiki/wiki/Nginx
    networking.firewall.allowedTCPPorts = [ 80 443 ];
    networking.nameservers = [ "45.134.88.88" ]; # ifog nameserver
    services.nginx = {
      enable = true;
      recommendedTlsSettings = true;
      recommendedProxySettings = true;
      virtualHosts."map.stadtfueralle.info" = {
        forceSSL = true;
        enableACME = true;
        locations = {
          "/static/".alias = staticFiles + "/";
          "/".proxyPass = "http://127.0.0.1:8000";
        };
      };
    };
    security.acme = {
      email = "news@durga.ch";
      acceptTerms = true;
      certs."map.stadtfueralle.info".email = "news@durga.ch";
    };
  };

  # generalCmds = [
  #   (pkgs.writeShellScriptBin "lxcSetGeoDbPsw" ''
  #     lxc exec geoDb -- psql -U sfa-user -c "ALTER USER \"sfa-user\" WITH PASSWORD '$DBPASS';" sfa-data
  #     lxc exec geoDb -- psql -U postgres -c "ALTER USER \"postgres\" WITH PASSWORD '$DBPASS';" sfa-data
  #   '')
  # ];
}
