{
  server = { pkgs, config, ... }: {
    boot.postBootCommands = ''
        # allow systemd to start user service after boot, no login needed
        loginctl enable-linger sfa-user
    '';
    environment.systemPackages = [ pkgs.atlas-app ];
    systemd.services.runserver = {
      enable = true;
      # preStart = ""; # sourcing here does not work, script gets a new environment
      script = "${pkgs.atlas-app.outPath}/bin/run-server";
      environment = {
        SW_FILE_PATH = pkgs.atlas-static + "/js/sw.js";
      };
      serviceConfig = {
        Restart = "always";
        EnvironmentFile = "/run/keys/exportVars";
        ExecStop = "${pkgs.utillinux}/bin/kill -9 $MAINPID";
        Type = "exec";
        User = "sfa-user";
        Group = "users";
      };
      wantedBy = [ "default.target" ];
    };
  };

  # generalCmds = [
  #   (pkgs.writeShellScriptBin "lxcSetGeoDbPsw" ''
  #     lxc exec geoDb -- psql -U sfa-user -c "ALTER USER \"sfa-user\" WITH PASSWORD '$DBPASS';" sfa-data
  #     lxc exec geoDb -- psql -U postgres -c "ALTER USER \"postgres\" WITH PASSWORD '$DBPASS';" sfa-data
  #   '')
  # ];
}
