nixos: postgresql10: sfa-atlas:
let
  staticFiles = sfa-atlas.outputs.packages.x86_64-linux.staticFiles;
  localHostAdr = "10.13.12.88";
  localHostDefaultGateway = "10.13.12.1";
  ifogHostAdr = "193.148.249.132";
  ifogHostDefaultGateway = "193.148.249.1";
  basicHostConf = ipAdr: gatewayAdr: {pkgs, ...}: {
    environment.systemPackages = [
      pkgs.openssl 
    ];
    networking = {
      firewall.enable = true;
      firewall.allowedTCPPorts = [ 22 ];
      defaultGateway = {
        address = gatewayAdr;
        interface = "eth0";
      };
      interfaces.eth0.ipv4.addresses = [{
        address = ipAdr;
        prefixLength = 24;
      }];
    };
    # fix for colmena apply
    # error: cannot add path '/nix/store/0m3....' because it lacks a valid signature
    nix.settings.trusted-users = [ "root" "@wheel" ];
    security.sudo = {
      enable = true;
      wheelNeedsPassword = false;
    };
    services.sshd.enable = true;
    users.users.sfa-user = {
      isNormalUser = true;
      uid = 1000;
      extraGroups = [ "keys" "wheel" ];
      openssh.authorizedKeys.keys = [
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOf6dgKch+BWz8MbOG6iovPhQQgrx4GRnm78HR3NnayP (none)"
      ];
    };
  };
  nixConfigurations = {
    localTesting = nixos.lib.nixosSystem {
      system = "x86_64-linux";
      modules = [
        "${nixos}/nixos/modules/virtualisation/qemu-vm.nix"
        (basicHostConf localHostAdr localHostDefaultGateway)
        ({...}: {
          users.extraUsers.root.password = "nixSagen";
          virtualisation.graphics = false;
          virtualisation.memorySize = 8192;
          virtualisation.diskSize = 2048;
          virtualisation.qemu = {
            networkingOptions = [
              "-nic tap,ifname=sfa-hostingTap,script=no,downscript=no,model=virtio-net-pci"
            ];
          };
        })
      ];
    };
    ifogIso = nixos.lib.nixosSystem {
      system = "x86_64-linux";
      modules = [
        "${nixos}/nixos/modules/installer/cd-dvd/iso-image.nix"
        "${nixos}/nixos/modules/profiles/headless.nix"
        "${nixos}/nixos/modules/profiles/minimal.nix"
        (basicHostConf ifogHostAdr ifogHostDefaultGateway)
        ({...}: {
          isoImage.isoName = "sfa-host-ifog.iso";
          services.udev.extraRules = ''
            SUBSYSTEM=="net", ACTION=="add", ATTR{address}=="ca:40:e9:df:c1:a2", NAME="eth0"
          '';
        })
      ];
    };
  };
  localQemu-imports = [
    ((import ./sfa-apps.nix).nginxTestConfig staticFiles)
  ] ++ nixConfigurations.localTesting._module.args.modules;
  ifog-imports = [
    ((import ./sfa-apps.nix).nginxIfogConfig staticFiles)
  ] ++ nixConfigurations.ifogIso._module.args.modules; 
in {
  inherit nixConfigurations;
  colmena = {
    meta = {
      nixpkgs = import nixos {
        system = "x86_64-linux";
        overlays = [
          (self: super: { pg10 = postgresql10; })
        ];
      };
      nodeNixpkgs = builtins.mapAttrs (name: value: value.pkgs) nixConfigurations;
      nodeSpecialArgs = builtins.mapAttrs (name: value: value._module.specialArgs) nixConfigurations;
    };
    defaults = {
      imports = [
        ((import ./sfa-apps.nix).server sfa-atlas.outputs.defaultPackage.x86_64-linux)
        (import ./db.nix).geoDbConfig
        (import ./db.nix).legacyDbConfig
      ];
      deployment = {
        targetUser = "sfa-user";
        keys."exportVars" = {
          keyFile = "/tmp/envIfog.sh";
            # text = "bangabanga";
          user = "sfa-user";
          group = "users";
          uploadAt = "pre-activation";
        };
        keys."legacyDbPass" = {
          keyFile = "/tmp/legacyDbPass";
          user = "postgres";
          group = "postgres";
          uploadAt = "pre-activation";
        };
      };
      boot.loader.grub.enable =false;
    };
    localQemu = {
      imports = localQemu-imports;
      deployment = {
        targetHost = localHostAdr;
      };
    };
    ifogQemu = {
      imports = ifog-imports;
      deployment = {
        targetHost = ifogHostAdr;
      };
      # fileSystems = {
      #   "/data" = {
      #     device = "/dev/disk/by-label/private";
      #     neededForBoot = true;
      #    };
      #   "/var" = {
      #     options = [ "noatime" ];
      #     device = "/dev/disk/by-label/var";
      #     neededForBoot = true;
      #   };
      #   "/nix/.rw-store" = lib.mkOverride 0 {
      #     options = lib.mkForce [];
      #     fsType = lib.mkForce "ext4";
      #     device = "/dev/mapper/pve-data";
      #     neededForBoot = true;
      #   };
      # };
      # swapDevices = [{
      #   device = "/dev/disk/by-label/swap";
      # }];
    };
  };
}
