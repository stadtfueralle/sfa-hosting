{
  description = "SFA-Hosting flake";
  inputs = {
    nixos.url = github:NixOS/nixpkgs/nixos-unstable;
    sfa-atlas.url = gitlab:stadtfueralle/sfa-atlas;
    # sfa-atlas.url = "/private/sfa-atlas";
    nixos-22_05.url = github:NixOS/nixpkgs/nixos-22.05;
    sfa-data.url = gitlab:stadtfueralle/sfa-data;
  };
  outputs = { self, nixos, nixos-22_05, sfa-atlas, sfa-data }:
  let
    project = "sfa-hosting";
    system = "x86_64-linux";
    pkgs = import nixos { inherit system; };
    pkgs-22_05 = import nixos-22_05 { inherit system; };
    postgres10 =  pkgs-22_05.postgresql_10.withPackages (p: [ p.postgis ]);
    host = import ./host.nix nixos postgres10 sfa-atlas;
  in {
    defaultPackage.${system} = sfa-atlas.sfaAtlasExec;
    packages.${system} = {
      localTestingQemu = host.nixConfigurations.localTesting.config.system.build.vm;
      ifogIso = host.nixConfigurations.ifogIso.config.system.build.isoImage;
    };
    colmena = host.colmena;
    devShell.${system} = pkgs.mkShell {
      buildInputs = with pkgs; [
        colmena
        cachix
        git
        openssh
        (pkgs.writeShellScriptBin "fillDb" ''
          ${pkgs.openssh}/bin/ssh sfa-user@10.13.12.88 "psql -U postgres sfa-data" < $1
        '')
        (pkgs.writeShellScriptBin "createBridge" ''
          ip tuntap add mode tap user cri name ${project}Tap
          ip link set ${project}Tap up
          ip link set ${project}Tap master br0
        '')
      ];
      shellHook =
        ''
          export PS1='✊\u@${project} \$ '
        '';
    };
  };
}
