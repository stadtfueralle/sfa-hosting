{
  description = "SFA-Hosting flake";
  inputs = {
    nixos.url = github:NixOS/nixpkgs/nixos-23.11;
    sfa-atlas.url = "git+https://codeberg.org/stadtfueralle/sfa-atlas";
    # sfa-atlas.url = "/private/sfa-atlas";
    # sfa-data.url = "git+https://codeberg.org/stadtfueralle/sfa-data";
    # needed for legacy db:
    nixos-22_05.url = github:NixOS/nixpkgs/nixos-22.05;
  };
  outputs = { self, nixos, nixos-22_05, sfa-atlas }:
  let
    project = "sfa-hosting";
    system = "x86_64-linux";
    pkgs = import nixos { inherit system; };
    pkgs-22_05 = import nixos-22_05 { inherit system; };
    postgresql10 =  pkgs-22_05.postgresql_10.withPackages (p: [ p.postgis ]);
    local = import ./localQemuHost.nix nixos;
    ifog = import ./ifog.nix nixos;
  in {
    defaultPackage.${system} = sfa-atlas.sfaAtlasExec;
    packages.${system} = {
      localTestingQemu = local.vm;
      localTestingQemuIso = local.iso;
      ifogIso = ifog.iso;
    };
    # colmena = host.colmena;
    colmena = {
      meta = {
        nixpkgs = import nixos {
          system = "x86_64-linux";
          overlays = [
            (self: super: {
              pg10 = postgresql10; 
              atlas-app = sfa-atlas.outputs.defaultPackage.x86_64-linux;
              atlas-static = sfa-atlas.outputs.packages.x86_64-linux.staticFiles;
            })
          ];
        };
        # nodeNixpkgs = builtins.mapAttrs (name: value: value.pkgs) nixConfigurations;
        # nodeSpecialArgs = builtins.mapAttrs (name: value: value._module.specialArgs) nixConfigurations;
      };
      defaults = {
        deployment = {
          targetUser = "sfa-user";
          targetPort = 62222;
        };
      };
      localQemuRaw = {
        imports = [ local.hwConfig local.basicConfig ];
        deployment = {
          targetHost = local.ips.address;
        };
      };
      localQemuFull = {
        imports = [
          (import ./db.nix).geoDbConfig
          (import ./db.nix).legacyDbConfig
          (import ./sfa-apps.nix).server
          local.nginxConfig
          local.basicConfig
          local.hwConfig
        ];
        deployment = {
          targetHost = local.ips.address;
          keys."legacyDbPass" = {
            keyFile = "/tmp/legacyDbPass";
            user = "postgres";
            group = "postgres";
            uploadAt = "pre-activation";
          };
          keys."exportVars" = {
            keyFile = "/tmp/envLocal.sh";
              # text = "bangabanga";
            user = "sfa-user";
            group = "users";
            uploadAt = "pre-activation";
          };
        };
      };
      ifogRaw = {
        imports = [
          ifog.basicConfig
          ifog.hwConfig
          (import ./db.nix).legacyDbConfig
          (import ./db.nix).geoDbConfig
        ];
        deployment = {
          targetHost = ifog.ips.address;
          keys."legacyDbPass" = {
            keyFile = "/tmp/legacyDbPass";
            user = "postgres";
            group = "postgres";
            uploadAt = "pre-activation";
          };
        };
      };
      ifogFull = {
        imports = [
          (import ./db.nix).geoDbConfig
          (import ./db.nix).legacyDbConfig
          (import ./sfa-apps.nix).server
          ifog.nginxConfig
          ifog.hwConfig
          ifog.basicConfig
        ];
        deployment = {
          targetHost = ifog.ips.address;
          keys."legacyDbPass" = {
            keyFile = "/tmp/legacyDbPass";
            user = "postgres";
            group = "postgres";
            uploadAt = "pre-activation";
          };
          keys."exportVars" = {
            keyFile = "/tmp/envIfog.sh";
              # text = "bangabanga";
            user = "sfa-user";
            group = "users";
            uploadAt = "pre-activation";
          };
        };
      };
    };
    devShell.${system} = pkgs.mkShell {
      buildInputs = with pkgs; [
        colmena
        cachix
        git
        openssh
        (pkgs.writeShellScriptBin "fillLegacyDb" ''
          ${pkgs.openssh}/bin/ssh sfa-user@$1 "psql -h /tmp/postgres10 -p 15432 -U postgres sfa-legacy" < $2
        '')
        (pkgs.writeShellScriptBin "fillDb" ''
          ${pkgs.openssh}/bin/ssh sfa-user@$1 "psql -U postgres sfa-data" < $2
        '')
        (pkgs.writeShellScriptBin "createBridge" ''
          ip tuntap add mode tap user cri name ${project}Tap
          ip link set ${project}Tap up
          ip link set ${project}Tap master br0
        '')
      ];
      shellHook =
        ''
          export PS1='✊\u@${project} \$ '
        '';
    };
  };
}
