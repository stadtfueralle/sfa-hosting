nixos:
let
  ips = {
    defaultGateway = "10.13.12.1";
    address = "10.13.12.88";
    nameservers = [ "9.9.9.9" ];
  };
  basicConfig = (import ./basicConf.nix ips);
  hwConfig = { pkgs, config, lib, ... }: {
    boot.loader.grub.enable = true;
    boot.loader.grub.device = "/dev/sda";
  };
in {
  inherit ips hwConfig basicConfig;
  nginxConfig = { pkgs, config, ... }: {
    networking.firewall.allowedTCPPorts = [ 80 ];
    networking.nameservers = ips.nameservers; # quad9 nameserver
    services.nginx = {
      enable = true;
      recommendedProxySettings = true;
      virtualHosts."localhost".locations = {
        "/static/".alias = pkgs.atlas-static + "/";
        "/".proxyPass = "http://localhost:8000";
      };
    };
  };
  vm = (nixos.lib.nixosSystem {
    system = "x86_64-linux";
    modules = [
      "${nixos}/nixos/modules/virtualisation/qemu-vm.nix"
      basicConfig
      ({config, ...}: {
        virtualisation.graphics = false;
        virtualisation.memorySize = 8192;
        virtualisation.diskSize = 2048;
        virtualisation.qemu = {
          networkingOptions = [
            "-nic tap,ifname=sfa-hostingTap,script=no,downscript=no,model=virtio-net-pci"
          ];
        };
      })
    ];
  }).config.system.build.vm;
  iso = (nixos.lib.nixosSystem {
    system = "x86_64-linux";
    modules = [
      "${nixos}/nixos/modules/installer/cd-dvd/iso-image.nix"
      basicConfig
      ({...}: {
        isoImage.isoName = "sfa-host.iso";
      })
    ];
  }).config.system.build.isoImage;
}

